from keras.preprocessing.text import Tokenizer
import config as conf
from utils import create_vocab_with_counts, build_character_indexes

from collections import Counter


def test_create_vocab_with_counts():

	min_count = 0
	input_raw =  "Il était une fois."
	mon_texte = [e for e in input_raw]
	counts = Counter(mon_texte)
	vocab = {k:v for (k, v) in counts.items() if v > min_count}

	assert vocab == {' ': 3,
	'.': 1,
	'I': 1,
	'a': 1,
	'e': 1,
	'f': 1,
	'i': 2,
	'l': 1,
	'n': 1,
	'o': 1,
	's': 1,
	't': 2,
	'u': 1,
	'é': 1}

def test_build_sentence():

	char2idx = {' ': 3,
	'.': 14,
	'I': 1,
	'UNKNOWN': 0,
	'a': 6,
	'e': 10,
	'f': 11,
	'i': 7,
	'l': 2,
	'n': 9,
	'o': 12,
	's': 13,
	't': 5,
	'u': 8,
	'é': 4}
	maxlen = 10
	input_raw =  "Il était une fois."
	mon_texte_clean = [e for e in input_raw]

	sentences = []
	for i in range(0, len(mon_texte_clean)-maxlen):
		sentences.append(mon_texte_clean[i:i+maxlen])

	tokenizer = Tokenizer(char_level=True, num_words=len(char2idx), lower=False)
	tokenizer.word_index = char2idx
	sentences_token = tokenizer.texts_to_sequences(sentences)

	train_sentences = sentences_token[:-1]
	target_sentences = sentences_token[1:]

	try:
		assert sentences == [['I', 'l', ' ', 'é', 't', 'a', 'i', 't', ' ', 'u'],
	['l', ' ', 'é', 't', 'a', 'i', 't', ' ', 'u', 'n'],
	[' ', 'é', 't', 'a', 'i', 't', ' ', 'u', 'n', 'e'],
	['é', 't', 'a', 'i', 't', ' ', 'u', 'n', 'e', ' '],
	['t', 'a', 'i', 't', ' ', 'u', 'n', 'e', ' ', 'f'],
	['a', 'i', 't', ' ', 'u', 'n', 'e', ' ', 'f', 'o'],
	['i', 't', ' ', 'u', 'n', 'e', ' ', 'f', 'o', 'i'],
	['t', ' ', 'u', 'n', 'e', ' ', 'f', 'o', 'i', 's']]
	except:
		raise ValueError("sentences", sentences)

	try:
		assert train_sentences ==  [[1, 2, 3, 4, 5, 6, 7, 5, 3, 8],
		[2, 3, 4, 5, 6, 7, 5, 3, 8, 9],
		[3, 4, 5, 6, 7, 5, 3, 8, 9, 10],
		[4, 5, 6, 7, 5, 3, 8, 9, 10, 3],
		[5, 6, 7, 5, 3, 8, 9, 10, 3, 11],
		[6, 7, 5, 3, 8, 9, 10, 3, 11, 12],
		[7, 5, 3, 8, 9, 10, 3, 11, 12, 7]]
	except:
		raise ValueError("train", train_sentences)

	try:
		assert target_sentences ==  [[2, 3, 4, 5, 6, 7, 5, 3, 8, 9],
		[3, 4, 5, 6, 7, 5, 3, 8, 9, 10],
		[4, 5, 6, 7, 5, 3, 8, 9, 10, 3],
		[5, 6, 7, 5, 3, 8, 9, 10, 3, 11],
		[6, 7, 5, 3, 8, 9, 10, 3, 11, 12],
		[7, 5, 3, 8, 9, 10, 3, 11, 12, 7],
		[5, 3, 8, 9, 10, 3, 11, 12, 7, 13]]
	except:
		raise ValueError(target_sentences)


	print("OK")

if __name__=="__main__":
	test_create_vocab_with_counts()
	test_build_sentence()
