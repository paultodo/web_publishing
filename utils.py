# -*- coding: utf-8 -*-
import io
import keras
from keras.callbacks import *
from collections import Counter
import numpy as np
import random
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
import os
from keras.utils import to_categorical
import pickle
import cloudpickle

def load_doc(filename):
    file = io.open(filename, 'r', encoding="utf-8")
    text = file.read()
    file.close()
    return text
    
def create_vocab_with_counts(text, min_count):
    counts = Counter(text)
    vocab = {k:v for (k, v) in counts.items() if v > min_count}
    return vocab

def build_character_indexes(vocab, unknown_token):
    idx2char = [x[0] for x in vocab]
    idx2char.insert(0, unknown_token)
    char2idx =dict([(w,i) for i, w in enumerate(idx2char)])
    return idx2char, char2idx

def replace_rare_char_with_unknwon_token(text, vocab, unknown_token):
    mon_texte_clean = [e if e in vocab.keys() else unknown_token for e in text]
    return mon_texte_clean

def build_sentence(mon_texte_clean, maxlen, char2idx):
    sentences = []
    for i in range(0, len(mon_texte_clean)-maxlen):
        sentences.append(mon_texte_clean[i:i+maxlen])

    tokenizer = Tokenizer(char_level=True, num_words=len(char2idx))
    tokenizer.word_index = char2idx
    sentences_token = tokenizer.texts_to_sequences(sentences)
    sentences_token = pad_sequences(sentences_token, maxlen=maxlen)
    
    train_sentences = sentences_token[:-1]
    target_sentences = sentences_token[1:]
    
    return sentences, train_sentences, target_sentences

def get_tr_te_indices(sentences, maxlen, multiple):
    chunk_size = maxlen*multiple
    all_indices = np.arange(len(sentences))
    chunks = [all_indices[x:x+chunk_size].tolist() for x in range(0, len(sentences), chunk_size)]

    indices_chunks = np.random.permutation(np.arange(len(chunks)-1))
    split = 0.8
    chunks_tr = indices_chunks[:int(len(indices_chunks)*split)]
    chunks_te = indices_chunks[int(len(indices_chunks)*split):]

    id_tr = []
    for indice in chunks_tr:
        id_tr.extend(chunks[indice])

    id_te = []
    for indice in chunks_te:
        id_te.extend(chunks[indice])

    return id_tr, id_te

def save_obj(obj_dir, obj_name, obj):
    if not os.path.exists(obj_dir):
        os.makedirs(obj_dir)
        
    with open(os.path.join(obj_dir, obj_name), 'wb') as f:
        pickle.dump(obj, f, protocol=2)
        
        
def load_obj(obj_dir, obj_name):
    with open(os.path.join(obj_dir, obj_name), 'rb') as f:
        res = pickle.load(f)
        return res

#### MODEL UTILS ####

class SampleSomeText(keras.callbacks.Callback):
    
    def __init__(self, seed_text, temp, char2idx, idx2char, maxlen, nb_char, unknown_token):
        super(SampleSomeText, self).__init__()
        self.seed_text = seed_text
        self.char2idx = char2idx
        self.idx2char = idx2char
        self.temp = temp
        self.nb_char = nb_char
        self.maxlen = maxlen
        self.unknown_token = unknown_token

    def on_epoch_end(self, eopch, logs={}):
        return generate_text(model=self.model,
                              char2idx=self.char2idx,
                              idx2char=self.idx2char,
                              maxlen=self.maxlen,
                              temp=self.temp,
                              seed_text=self.seed_text,
                              nb_char=self.nb_char,
                              unknown_token=self.unknown_token)


def sample(preds, temperature=1.0):
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    preds = np.nan_to_num(preds)
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    preds = preds.astype(np.float64)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)

def generate_text(model, char2idx, idx2char, maxlen, temp=1, seed_text="e",
                    nb_char=400, print_text=True, unknown_token="UNKNOWN"):

  
    #print('----- Generating with seed: ' + seed_text )
    print('-----temperature : {}'.format(temp))
    sampled = seed_text
    sentence = seed_text
    for i in range(nb_char):
        encoded = [char2idx[char] for char in sentence]
        encoded = pad_sequences([encoded], maxlen=maxlen)
        preds = model.predict(encoded, verbose=0)
        preds = np.squeeze(preds, axis=0)
        next_index=sample(preds[-1], temperature=temp)
        while next_index == char2idx[unknown_token]:
            next_index = sample(preds[-1], temperature=temp)
        
        next_char = idx2char[next_index]
        sentence = sentence[1:] + next_char
        sampled += next_char
        
    if print_text == True:
        print(sampled)
    
    #sampled = sampled.replace('\n', '<br>')
    return sampled



class topModel(object):
    def __init__(self, model_dir, model_name, model_weights_name, model_fn, model_fn_cudnn, model_params, cudnn=True):
        self.model_dir = model_dir
        self.model_weights_name = model_weights_name
        self.model_fn = model_fn
        self.model_fn_cudnn = model_fn_cudnn
        self.model_params = model_params
        self.keras_model = None
        self.is_built = False
        self.is_fitted = False
        self.cudnn = cudnn
        self.model_name = model_name
        
    def build(self):
        if not self.is_built:
            if self.cudnn == True:
                print("Using CUDNN")
                self.keras_model = self.model_fn_cudnn(**self.model_params)
            elif self.cudnn == False:
                print("Not using CUDNN")
                self.keras_model = self.model_fn(**self.model_params)
                
            print("built")
            self.is_built = True
            
    def save(self):
        if not os.path.exists(self.model_dir):
            os.makedirs(self.model_dir)
            
        with open(os.path.join(self.model_dir, 'model_fn.pkl'), 'wb') as f:
            cloudpickle.dump(self.model_fn, f, protocol=2)
        
        with open(os.path.join(self.model_dir, 'model_fn_cudnn.pkl'), 'wb') as f:
            cloudpickle.dump(self.model_fn_cudnn, f, protocol=2)
        
        with open(os.path.join(self.model_dir, 'model_params.pkl'), 'wb') as f:
            cloudpickle.dump(self.model_params, f, protocol=2)
        

        if self.keras_model is not None:
            # save model with optimizer states etc.
            keras.models.save_model(self.keras_model, os.path.join(self.model_dir, self.model_name))
            # save weights
            self.keras_model.save_weights(os.path.join(self.model_dir, self.model_weights_name))
                
    def _train_generator(self, X, y, ind_tr, batch_size, maxlen, nb_char):
        while True: 
            Xb = np.zeros((batch_size, maxlen))
            Yb = np.zeros((batch_size, maxlen, nb_char))
            indexs = np.random.choice(ind_tr, size=batch_size, replace=False)
            for b in range(batch_size):
                # try:
                Xb[b,:] = X[indexs[b]]
                Yb[b,:,:] = to_categorical(y[indexs[b]], num_classes=nb_char)
                # except:
                    # raise ValueError("b :{}, index : {}".format(b, indexs))
            yield Xb, Yb

    def _valid_generator(self, X, y, ind_te, batch_size, maxlen, nb_char):
        while True: 
            Xb = np.zeros((batch_size, maxlen))
            Yb = np.zeros((batch_size, maxlen, nb_char))
            indexs = np.random.choice(ind_te, size=batch_size, replace=False)
            for b in range(batch_size):
                Xb[b,:] = X[indexs[b]]
                Yb[b,:,:] = to_categorical(y[indexs[b]], num_classes=nb_char)
            yield Xb, Yb

   
    def train(self, X, y, ind_tr, ind_te, loss,
             nb_epochs, batch_size, optimizer, writer_callback=None, metrics=None, overwrite=False):
        
        if os.path.exists(self.model_dir):
            if overwrite:
                shutil.rmtree(self.model_dir, ignore_erros=True)
                
        if not os.path.exists(self.model_dir):
            os.makedirs(self.model_dir)

        if not self.is_built:
            self.build()

        self.keras_model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

        callbacks = [
            ModelCheckpoint(os.path.join(self.model_dir, self.model_weights_name), save_best_only=True),
            CSVLogger(os.path.join(self.model_dir, 'train_log.csv')),
            ReduceLROnPlateau(factor=0.5, patience=1),
            EarlyStopping(patience=2)
        ]

        if writer_callback:
            callbacks += [writer_callback]
        train_generator = self._train_generator(X, y, ind_tr, batch_size, self.model_params["maxlen"], self.model_params["nb_char"])
        valid_generator = self._valid_generator(X, y, ind_te, batch_size, self.model_params["maxlen"], self.model_params["nb_char"])


        self.keras_model.fit_generator(train_generator, epochs=nb_epochs,
                                       steps_per_epoch=len(ind_tr)//batch_size,
                                       callbacks=callbacks, validation_data=valid_generator,
                                       validation_steps=len(ind_te)//batch_size, workers=1)

        self.is_fitted = True


    def load_best_weights(self):
        self.keras_model.load_weights(os.path.join(self.model_dir, self.model_weights_name))

    @classmethod
    def restore_model(cls, model_dir, model_name, model_weights_name, cudnn=True):
        with open(os.path.join(model_dir, 'model_fn.pkl'), 'rb') as f:
            model_fn = cloudpickle.load(f)

        with open(os.path.join(model_dir, 'model_fn_cudnn.pkl'), 'rb') as f:
            model_fn_cudnn = cloudpickle.load(f)

        with open(os.path.join(model_dir, 'model_params.pkl'), 'rb') as f:
            model_params = cloudpickle.load(f)
        
        model = cls(model_dir = model_dir,
                    model_fn = model_fn,
                    model_fn_cudnn = model_fn_cudnn,
                    model_params = model_params,
                    cudnn = cudnn,
                    model_name = model_name,
                    model_weights_name = model_weights_name
                    )

        model.build()

        model_path = os.path.join(model_dir, model_name)

        if os.path.exists(model_path):
            model.keras_model = keras.models.load_model(model_path)
            print('loading model {}'.format(model_path))
            model.is_fitted = True
            return model


def load_trained_model(cls, model_dir, model_name, model_weights_name, cudnn=True):
    with open(os.path.join(model_dir, 'model_fn.pkl'), 'rb') as f:
        model_fn = cloudpickle.load(f)

    with open(os.path.join(model_dir, 'model_fn_cudnn.pkl'), 'rb') as f:
        model_fn_cudnn = cloudpickle.load(f)
        
    with open(os.path.join(model_dir, 'model_params.pkl'), 'rb') as f:
        model_params = cloudpickle.load(f)

    model = cls(model_dir = model_dir,
                model_fn = model_fn,
                model_fn_cudnn = model_fn_cudnn,
                model_params = model_params,
                cudnn = cudnn,
                model_name = model_name,
                model_weights_name = model_weights_name)
       

    model.build()

    model_weights_path = os.path.join(model_dir, model_weights_name)
    if os.path.exists(model_weights_path):
        model.keras_model.load_weights(model_weights_path)
        print('loading weights {}'.format(model_weights_path))
        model.is_fitted = True
        return model