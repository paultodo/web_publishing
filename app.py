# -*- coding: utf-8 -*-
import os
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import plotly.graph_objs as go

from utils import topModel, SampleSomeText, generate_text, load_trained_model, load_obj
import tensorflow as tf

import config as conf

app = dash.Dash()
server = app.server

app.layout = html.Div([
	html.Div(
		className="app-header",
		children=[
			html.Div('Sample text',
			style={'textAlign': 'center'},
			className="app-header--title")
			]
		),

	html.Div(children=[
		html.Label('Enter seed text: '),
		dcc.Input(id='seed-text', placeholder='Seed text', type='text'),
		
		html.Label('Temperature: '),
		dcc.Input(id='temperature', placeholder='Temperature', type='text'),
		
		html.Label('Number of characters: '),
		dcc.Input(id='nb_characters', placeholder='Characters', type='number'),

		html.Button('Click Me', id='my-button'),
		html.Div(id='generated', style={'textAlign': 'left', 'white-space': 'pre-line'})
	], style={'textAlign': 'center'}),
])



@app.callback(
Output(component_id='generated', component_property='children'),
[Input('my-button', 'n_clicks')],
state=[State(component_id='seed-text', component_property='value'),
		State(component_id='temperature', component_property='value'),
		State(component_id='nb_characters', component_property='value')		
		])
def generate_some_text(number_of_times_button_has_clicked, seed_text, temperature=1., nb_characters=400):
	if seed_text is not None and seed_text is not '':
		temperature = float(temperature)
		nb_characters = int(nb_characters)
		
		try:
			print(type(seed_text))
			with graph.as_default():
				res = generate_text(new.keras_model, char2idx, idx2char, conf.MAXLEN,
					temp=temperature, seed_text=seed_text, nb_char=nb_characters,
					print_text=False, unknown_token="UNKNOWN")

				print(res)
				return res

		except Exception as e:
			return e


port = int(os.environ.get("PORT", 6010))
print(port)
char2idx = load_obj(os.path.join(conf.PROCESSED_DATA, conf.MODEL_NAME), "char2idx")
idx2char = load_obj(os.path.join(conf.PROCESSED_DATA, conf.MODEL_NAME), "idx2char")

new = load_trained_model(cls=topModel,
                     model_dir=os.path.join(conf.SAVE_MODEL_FOLDER, conf.EXP_NAME),
                     model_weights_name=conf.WEIGHTS_NAME,
                     model_name=conf.MODEL_NAME,
                     cudnn=False)

new.keras_model._make_predict_function()
graph = tf.get_default_graph()

if __name__ == "__main__":
	app.run_server(host='0.0.0.0', port=port)
