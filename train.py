# -*- coding: utf-8 -*-

import io
import sys
import os
import keras
import pickle

from utils import load_doc, create_vocab_with_counts, build_character_indexes, replace_rare_char_with_unknwon_token, build_sentence, get_tr_te_indices
from utils import topModel, SampleSomeText, generate_text, load_trained_model, load_obj, save_obj
from models import make_model_cudnn, make_model

import config as conf

import test

test.test_build_sentence()

mon_text = load_doc(os.path.join(conf.DATA_FOLDER, conf.FILENAME))
vocab = create_vocab_with_counts(mon_text, conf.MIN_COUNT)
idx2char, char2idx  = build_character_indexes(vocab, conf.UNKNOWN_TOKEN)
mon_texte_clean = replace_rare_char_with_unknwon_token(mon_text, vocab, conf.UNKNOWN_TOKEN)
sentences, train_sentences, target_sentences = build_sentence(mon_texte_clean, conf.MAXLEN, char2idx)
ind_tr, ind_te = get_tr_te_indices(sentences, conf.MAXLEN, conf.MULTIPLE)
ind_tr, ind_te = get_tr_te_indices(sentences, conf.MAXLEN, conf.MULTIPLE)
nb_char = len(char2idx)

# save data objects
save_obj(os.path.join(conf.PROCESSED_DATA, conf.MODEL_NAME), "char2idx", char2idx)
save_obj(os.path.join(conf.PROCESSED_DATA, conf.MODEL_NAME), "idx2char", idx2char)

# create sampler callback
sampler_seed = ''.join(sentences[ind_tr[100]]+sentences[ind_tr[100+conf.MAXLEN]])
sampler = SampleSomeText(seed_text=sampler_seed, temp=0.5, char2idx=char2idx, idx2char=idx2char,
                         maxlen=conf.MAXLEN, nb_char=400, unknown_token=conf.UNKNOWN_TOKEN)
# build model
writer = topModel(model_dir = os.path.join(conf.SAVE_MODEL_FOLDER, conf.EXP_NAME),
            model_weights_name = conf.WEIGHTS_NAME,
            model_name = conf.MODEL_NAME,
            model_fn_cudnn=make_model_cudnn,
            model_fn=make_model,
            model_params={"maxlen":conf.MAXLEN, "nb_char":nb_char,
                          "embedding_dim":conf.EMBEDDING_DIM, "nb_units":conf.NB_UNITS})

# train model
writer.train(train_sentences, target_sentences, ind_tr, ind_te, loss=keras.losses.categorical_crossentropy,
             nb_epochs=1, batch_size=256,
             optimizer=keras.optimizers.Adam(),
             writer_callback=sampler)

writer.load_best_weights()

writer.save()