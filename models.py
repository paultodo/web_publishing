from keras.layers import LSTM, Input, Embedding, Dense, Dropout, Bidirectional, BatchNormalization, Bidirectional
from keras.layers import TimeDistributed, Lambda
from keras.layers import CuDNNLSTM
from keras.models import Model
from keras import activations
from keras.layers import Layer

class TiedEmbeddingsTransposed(Layer):
    """Layer for tying embeddings in an output layer.
    A regular embedding layer has the shape: V x H (V: size of the vocabulary. H: size of the projected space).
    In this layer, we'll go: H x V.
    With the same weights than the regular embedding.
    In addition, it may have an activation.
    # References
        - [ Using the Output Embedding to Improve Language Models](https://arxiv.org/abs/1608.05859)
    """

    def __init__(self, tied_to=None,
                 activation=None,
                 **kwargs):
        super(TiedEmbeddingsTransposed, self).__init__(**kwargs)
        self.tied_to = tied_to
        self.activation = activations.get(activation)

    def build(self, input_shape):
        self.transposed_weights = K.transpose(self.tied_to.weights[0])
        self.built = True

    def compute_mask(self, inputs, mask=None):
        return mask

    def compute_output_shape(self, input_shape):
        return input_shape[0], K.int_shape(self.tied_to.weights[0])[0]

    def call(self, inputs, mask=None):
        output = K.dot(inputs, self.transposed_weights)
        if self.activation is not None:
            output = self.activation(output)
        return output

    def get_config(self):
        config = {'activation': activations.serialize(self.activation)
                  }
        base_config = super(TiedEmbeddingsTransposed, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

def make_model_cudnn(maxlen, nb_char, embedding_dim, nb_units):
    ## build model
    input_sentence = Input(shape=(maxlen,), dtype='int32', name="input")
    embedding_layer = Embedding(nb_char, embedding_dim, trainable=True)(input_sentence)
    lstm1 = CuDNNLSTM(nb_units, stateful=False, input_shape=(maxlen, embedding_dim), return_sequences=True)
    lstm2 = CuDNNLSTM(nb_units, stateful=False, input_shape=(maxlen, embedding_dim), return_sequences=True)
    output = lstm1(embedding_layer)
    output = lstm2(output)
    output = TimeDistributed(Dense(nb_char, activation='softmax'))(output)
    model = Model(inputs=input_sentence, outputs=output)
    # add layer
    # add dropout
    # add batchnormalization
    return model

def make_model(maxlen, nb_char, embedding_dim, nb_units):
    ## build model
    input_sentence = Input(shape=(maxlen,), name="input")
    embedding_layer = Embedding(nb_char, embedding_dim, trainable=True)(input_sentence)
    lstm1 = LSTM(nb_units, stateful=False, input_shape=(maxlen, embedding_dim), return_sequences=True, dropout=0)
    lstm2 = LSTM(nb_units, stateful=False, input_shape=(maxlen, embedding_dim), return_sequences=True, dropout=0)
    output = lstm1(embedding_layer)
    output = lstm2(output)
    output = TimeDistributed(Dense(nb_char, activation='softmax'))(output)
    model = Model(inputs=input_sentence, outputs=output)
    # add layer
    # add dropout
    # add batchnormalization
    return model

def make_model_cudnn_tie(maxlen, nb_char, embedding_dim, nb_units):
    ## build model
    EMBEDDING_LAYER = Embedding(nb_char, embedding_dim, trainable=True)
    LSTM1 = CuDNNLSTM(nb_units, stateful=False, input_shape=(maxlen, embedding_dim), return_sequences=True)
    LSTM2 = CuDNNLSTM(nb_units, stateful=False, input_shape=(maxlen, embedding_dim), return_sequences=True)
    
    input_sentence = Input(shape=(maxlen,), dtype='int32', name="input")
    embedded_sentences = EMBEDDING_LAYER(input_sentence)
    output = LSTM1(embedded_sentences)
    output = LSTM2(output)
    output = TimeDistributed(TiedEmbeddingsTransposed(tied_to=EMBEDDING_LAYER,
                                                  activation='softmax'))(output)
    model = Model(inputs=input_sentence, outputs=output)
    # add layer
    # add dropout
    # add batchnormalization
    return model