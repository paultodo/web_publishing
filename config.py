# prefix
PREFIX = "v0.1"

# data config
DATA_FOLDER = "data"
FILENAME = "input_PT.txt"

PROCESSED_DATA = "processed_data"
MIN_COUNT = 100
UNKNOWN_TOKEN  = "UNKNOWN"
MAXLEN = 70
MULTIPLE = 20

# model architecture config
EMBEDDING_DIM = 32
NB_UNITS = 512

# model save config
SAVE_MODEL_FOLDER = "model_weights_params"
WEIGHTS_NAME = "model.h5"
MODEL_NAME = "model.mod"
EXP_NAME = "{}xp_{}emb_{}lstmunits_{}".format(PREFIX, EMBEDDING_DIM, NB_UNITS, FILENAME.split(".")[0])
